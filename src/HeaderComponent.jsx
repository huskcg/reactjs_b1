import React, { Component } from "react";

export default class HeaderComponent extends Component {
  render() {
    return (
      <div className="bg-dark">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark container w-75">
          <a className="navbar-brand w-50 text-left" href="/#">
            Start Bootstrap
          </a>
          <div
            className="collapse navbar-collapse w-50 justify-content-end"
            id="navbarNav"
          >
            <ul className="navbar-nav ">
              <li className="nav-item active">
                <a className="nav-link" href="/#">
                  Home <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#">
                  About
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#">
                  Services
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/#">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
