import React, { Component } from "react";

export default class CardComponent extends Component {
  render() {
    return (
      <div>
        <div className="container w-75 mt-3 pl-0 pr-0 ">
          <div className="row gx-2">
            <div className="col">
              <div className="card" style={{ width: "100%" }}>
                <div
                  style={{
                    backgroundColor: "#CBCBCB",
                    width: "100%",
                    height: "150px",
                    lineHeight: "150px",
                  }}
                >
                  500x235
                </div>
                <div className="card-body mb-5">
                  <h4 className="card-title">Card title</h4>
                  <p className="card-text">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Debitis est fugit ipsum.
                  </p>
                </div>
                <div className="card-footer">
                  <a href="/#" className="btn btn-primary">
                    Find Out More!
                  </a>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="card" style={{ width: "100%" }}>
                <div
                  style={{
                    backgroundColor: "#CBCBCB",
                    width: "100%",
                    height: "150px",
                    lineHeight: "150px",
                  }}
                >
                  500x235
                </div>
                <div className="card-body mb-5">
                  <h4 className="card-title">Card title</h4>
                  <p className="card-text">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Debitis est fugit ipsum.
                  </p>
                </div>
                <div className="card-footer">
                  <a href="/#" className="btn btn-primary">
                    Find Out More!
                  </a>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="card" style={{ width: "100%" }}>
                <div
                  style={{
                    backgroundColor: "#CBCBCB",
                    width: "100%",
                    height: "150px",
                    lineHeight: "150px",
                  }}
                >
                  500x235
                </div>
                <div className="card-body mb-5">
                  <h4 className="card-title">Card title</h4>
                  <p className="card-text">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Debitis est fugit ipsum.
                  </p>
                </div>
                <div className="card-footer">
                  <a href="/#" className="btn btn-primary">
                    Find Out More!
                  </a>
                </div>
              </div>
            </div>
            <div className="col">
              <div className="card" style={{ width: "100%" }}>
                <div
                  style={{
                    backgroundColor: "#CBCBCB",
                    width: "100%",
                    height: "150px",
                    lineHeight: "150px",
                  }}
                >
                  500x235
                </div>
                <div className="card-body mb-5">
                  <h4 className="card-title">Card title</h4>
                  <p className="card-text">
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Debitis est fugit ipsum.
                  </p>
                </div>
                <div className="card-footer">
                  <a href="/#" className="btn btn-primary">
                    Find Out More!
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
